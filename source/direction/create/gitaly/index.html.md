---
layout: markdown_page
title: "Category Vision - Gitaly"
---

- TOC
{:toc}

## Gitaly

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

Gitaly is a Git RPC service for handling all the Git calls made by GitLab. Until mid 2018, GitLab application relied on direct disk access to Git repositories, performing Git operations with either Rugged (libgit2 wrapper) or by shelling out to Git directly. At scale, this meant using NFS to make the repositories available to every application server. NFS adds latency and has opaque failure modes which are hard to debug in production. Furthermore, using multiple interfaces for Git makes instrumentation and caching difficult. In late 2016, GitLab began building Gitaly, a gRPC service that would become the interface through which the GitLab application interacts with Git repositories, and in mid 2018, GitLab completed this process for GitLab.com and unmounted NFS from GitLab.com application servers.

The vision for Gitaly is to make storing and accessing Git repositories – large and small – fast and reliable. Thus, performance and availability are key features. Because of Gitaly's direct interaction with Git, the we are working to improve Git too, to make using extremely large repositories fast and easy through features like partial clone.

- See a high-level [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Gitaly&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Gitaly)
- [Overall Vision](/direction/create/)

Interested in joining the conversation for this category? Please join us in our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/710) where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

## Target Audience and Experience

<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

- **In progress (ETA 12.1):** [High Availability Gitaly (Beta): Naïve Replication](https://gitlab.com/groups/gitlab-org/-/epics/289)

    Currently there is no way to run GitLab in a HA configuration without NFS. This is preventing GitLab from being in the AWS marketplace and from running GitLab in a HA configuration in Kubernetes. The first phase will be a beta focused on data replication, but without fail over workflows or monitoring node health.
    
- **Next:** [High Availability Gitaly (Beta): Automatic Failover](https://gitlab.com/groups/gitlab-org/-/epics/1185)

    Assuming naïve replication (eventual consistency, no optimizations) provides replication delays that are within the ballpark of less than 1 minute at the 99th percentile, we will begin focussing on failure detection and failover which is a fundamental feature of providing a usable HA solution.

## Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [GitHub.com](https://github.com)
- [GitHub Enterprise](https://github.com/enterprise)
- [Bitbucket Cloud](https://bitbucket.org/product/)
- [Bitbucket Server](https://bitbucket.org/product/)
- [Perforce](https://perforce.com)
- [CVS: Concurrent Versions System](https://nongnu.org/cvs/)
- [SVN: Apache Subversion](https://subversion.apache.org/)

## Business Opportunity

<!-- This section should highlight the business opportunity highlighted by the particular category. -->

Git is the market leading version control system, used by the majority of individuals and enterprises. According to the [2018 Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) over 88% of respondents use Git.

According to a [2016 Bitrise survey](https://blog.bitrise.io/state-of-app-development-2016#self-hosted), 62% of apps hosted by SaaS provider were hosted in GitHub, and 95% of apps are hosted in by a SaaS provider.

## Analyst Landscape

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

- [Native support for large files](https://gitlab.com/groups/gitlab-org/-/epics/958) is very important to companies that need to version large binary assets, like game studios. These companies primarily use Perforce because Git LFS provides poor experience with complex commands and careful workflows needed to avoid large files entering the repository. GitLab has been supporting work to provide a more native large file workflow based on promiser packfiles which will be very significant to analysts and customers when the feature is ready.

## Top Customer Success/Sales issue(s)

<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [High Availability Gitaly](https://gitlab.com/groups/gitlab-org/-/epics/842) is need to allow customers to avoid needing NFS to achieve a highly available GitLab instance. The network latency of any network based file system, like NFS, EFS, Gluster, will negatively impact Git performance because of Git's disk access requirements. It is important to customers want to run an HA GitLab instance that we provide a better way.
  - [Naive replication](https://gitlab.com/groups/gitlab-org/-/epics/289)
  - [Automatic failover](https://gitlab.com/groups/gitlab-org/-/epics/1185)
  - [HA Praefect](https://gitlab.com/groups/gitlab-org/-/epics/1186)
  - [Initial replication](https://gitlab.com/groups/gitlab-org/-/epics/1187)
- [Gitaly N+1 issues](https://gitlab.com/groups/gitlab-org/-/epics/827) are a bad Git access pattern that results in bad performance of the GitLab application. Gitaly needs to work with each time to replace these bad implementations.
- [Native support for extremely large repositories](https://gitlab.com/groups/gitlab-org/-/epics/915) prevents existing customers and prospects from being able to migrate enormous repositories from Perforce or SVN to Git. It is frequently requested and many organizations want to standardize on a single version control system and tool like GitLab across all projects.

## Top user issue(s)

<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Users do not see Gitaly as a distinct feature or interface of GitLab. Git performance is the most significant user facing area where improvements are frequently requested, however the source of the performance problem can vary significantly.

## Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [High Availability Gitaly](https://gitlab.com/groups/gitlab-org/-/epics/842) is important to the Distribution team so that we can offer a GitLab Helm chart that supports high availability. It is also important to the Production team so that we can consider deploying GitLab.com in Kubernetes.

## Top Vision Item(s)

<!-- What's the most important thing to move your vision forward?-->

- [Native support for large files](https://gitlab.com/groups/gitlab-org/-/epics/958) prevents existing customers and prospects being able to migrate repositories with large files to Git. Git LFS isn't a sufficient solution for these organisations in comparison with the native support of other version control systems. The most pressing problem is avoiding the need to download enormous amounts of data, and not having to remember to use different commands for different files so as not to make life worse for everyone.
- [Deduplicated Git objects](https://gitlab.com/groups/gitlab-org/-/epics/1174) provides significant performance improvements for forking, and cost savings for customers using a forking workflow. The savings are increasingly significant as the size of the project increase and the number of forks increases.
- [Improved clone performance](https://gitlab.com/groups/gitlab-org/-/epics/1117) will benefit the most common use case of cloning a project, but will also provide the foundation for accelerating partial clones where the clone may be filtered by blob size or path. This approach may also accelerate CI clones too.
