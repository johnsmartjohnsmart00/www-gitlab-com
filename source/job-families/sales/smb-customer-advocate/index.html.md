---
layout: job_family_page
title: "SMB Customer Advocate"
---

## Responsibilities
- Experience managing an inbound request queue and assisting prospects and customers with online chat, email, and phone correspondence.
- Familiarity with the needs of software development and IT operations teams.
- Basic understanding of application lifecycle management including the technology commonly used in application lifecycle management.
- Basic understanding of DevOps practices, including the cultural shift it represents and the benefits to building and shipping software upon adopting those practices.
- Experience in a role that puts customer needs above all else.
- Product demo experience.
- Working knowledge of software development methodologies such as Waterfall, Agile, and Conversational Development.
- Comfortable with frequent client phone calls to explain hard-to-understand concepts and deployment options.

## Requirements
- Excellent spoken and written English
- You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
- Affinity with software and the software development process
- Passionate about technology and learning more about GitLab
- 3+ years experience in sales, marketing, or customer service
- Experience with CRM and email automation software is highly preferred
- An understanding of B2B software, Open Source software, and the developer product space is preferred
- Be ready to learn how to use GitLab and Git
- You share our values, and work in accordance with those values

## SMB Operations

**Renewal/account intake:** 

Renewal/account intake involves responding to customers that contact GitLab through the following channels:
1. Salesforce Cases (where our renewals@gitlab.com live)
2. Zendesk
3. Direct email (customers, partners)

The instructions below describe the basic process for properly handling inquiries from each of these channels.

**Salesforce Cases** (proactively monitor, check at least three times daily):
* Navigate to the "Cases" tab in [Salesforce](https://gitlab.my.salesforce.com/).
* Create custom view for managing all cases assigned to your alias specifically. (Instructions for creating new view coming soon.)
* Review [this view](https://gitlab.my.salesforce.com/500?fcf=00B61000004LtSU) of unassigned cases and reassign to yourself any within your [territory](https://about.gitlab.com/handbook/business-ops/#territories).
* Navigate to your cases view in SFDC, filter for most recent cases. With any open cases, please work according to the following steps:
  *  Open case > Open account. 
  *  If the account has an owner that is not you or is Sales Admin and not within your territory, navigate back to case, reassign it to the proper account owner based on territory, and chatter them.   
  *  If the account is owned by you or Sales Admin and is within your territory, check for activity history on the case, account, and contact. 
  *  If a colleague has responded to the case or is in touch with the contact, reassign the case to them. 
  *  If there is no activity history, respond to case and continue communication with customer via email.
  *  After responding, update case status from "New" to "Closed". This ensures only untouched cases will remain in your queue.

**Zendesk** (proactively monitor, check at least three times daily):
* Navigate to [Zendesk](https://gitlab.zendesk.com/).
* Click "Views" and choose "Upgrades, Renewals & AR (refunds)".
  * Tickets are automatically sorted from greatest urgency to least, start from tickets at the top of the list.
  * Open ticket, then review the activity log. If a sales colleague has already been in touch and this customer is awaiting another reply, ping your colleague on Slack with a link to this ticket if an [SLA breach](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/zendesk_admin.html#service-level-agreements-set-as-business-rules) is imminent. 
  * If there is no activity history on the ticket, use the contact's email address from Zendesk and check SFDC to see who ticket should be routed to. 
  * When the account is in your territory, reassign the ticket to yourself (if not already in your name), and respond and/or update the ticket status accordingly. 

Ticket status can be updated in the following ways:
* "*Submit as Pending*" if you have written a response to a prospect and are expecting their response.
* "*Submit as Solved*" if the ticket has been resolved and can be closed (i.e. no response is expected from the prospect).
* "*Submit as Open*" if the account is outside of SMB segmentation; make sure to cc the appropriate account owner within the ticket. Ping the account manager on Slack notifying them of the active ticket that requires their response.
  * If the account is requesting a refund or billing support, disposition the ticket to the Accounts/Receivable Refunds form on the left panel and "*Submit as Open*". 

**Direct email** (proactively monitor, check at least three times daily):
*  If a Customer or Partner emails you directly, check SFDC for account ownership and introduce the correct colleague.
*  If the account is in your territory, continue correspondence via email.  


## Hiring Process
Avoid the confidence gap; you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](/handbook/hiring).

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
  1. Why do you want to work as an SMB Customer Advocate specifically for GitLab?
  2. What is your greatest professional accomplishment?
  3. What motivates you?
  4. How would you describe your knowledge of Developer Tools and/or Open Source Software?
  5. What do you see as the main difference between supporting a prospect through their buyer's journey versus managing a sales cycle?
  6. Tell me about the last time you helped a customer or prospective customer with a problem or challenge they had.
* Selected candidates will be invited to the following
  1. A phone or video conference screen with the recruiter. This takes 30 minutes and helps our recruiting team align your interests and qualifications with the right opening on the sales team.
  1. A video conversation with the hiring manager. This takes 30 minutes and helps the sales manager understand what you want to do next in your career and how that might intersect with GitLab and the Commercial sales team.
  1. A video conversation reviewing your professional experience chronologically. This takes from 45 min to an hour and allows your hiring manager to understand the similarities and differences of past roles you’ve had to the role we are considering for you.
  1. A video conversation with a potential colleague. This takes 45 minutes and helps us understand your sales experience, style, and skills.
  1. A mock GitLab customer call. The mock call itself is limited to 15 minutes. You are give a few minutes to ask any final questions before going into the mock call. After the mock call we use the remainder of the half hour to understand feedback, how it felt to sell GitLab, and ask any questions we have remaining about your candidacy. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Compensation

Your compensation will be 50% base, 50% bonus based on overall team performance in achieving IACV goals in the SMB market. See our [market segmentation](/handbook/sales/#market-segmentation) for detail on how the SMB market is defined at GitLab. Also see the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/).

